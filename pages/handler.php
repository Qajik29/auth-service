<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(!empty($_POST["fname"]) && !empty($_POST["lname"]) && !empty($_POST["email"]) && !empty($_POST["password"])) {
        $user_fname = clear_data($_POST["fname"]);
        $user_lname = clear_data($_POST["lname"]);
        $user_email = clear_data($_POST["email"]);
        $user_password = clear_data($_POST["password"]);
        $stmt = $pdo->prepare("SELECT fname FROM users WHERE email=? LIMIT 1");
        $stmt->execute([$user_email]);
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($user)) {
            $_SESSION["user_already_exists"] = 'User already exists';
            header('Location: ?page=signup');
        }
        else {
            try{
                $stmt = $pdo->prepare("INSERT INTO users (fname, lname, email, password) VALUES (:fname, :lname, :email, :password)");
                $data = array(
                    ':fname' => $user_fname,
                    ':lname' => $user_lname,
                    ':email' => $user_email,
                    ':password' => $user_password
                );
                $res = $stmt->execute($data);
                header('Location: ?page=dashboard');
            } catch (Exception $e){
                $str= filter_var($e->getMessage(), FILTER_SANITIZE_STRING);
                $_SESSION['_msg_err'] = $str;
            }
        }
    } else {
        header('Location: ?page=login');
    }
}else{
    header('Location: ?page=login');
}