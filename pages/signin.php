<?php
if(isset($_SESSION["authenticated"])){
    header('Location: ?page=dashboard');
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(!empty($_POST["email"] && !empty($_POST["password"]))) {
        $user_email = clear_data($_POST["email"]);
        $user_password = clear_data($_POST["password"]);
        $stmt = $pdo->prepare("SELECT fname FROM users WHERE email=? AND password=? LIMIT 1");
        $stmt->execute([$user_email, $user_password]);
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($user[0])) {
            $_SESSION["user_name"] = $user[0]['fname'];
            $_SESSION["authenticated"] = 'true';
            header('Location: ?page=dashboard');        }
        else {
            header('Location: ?page=login');
        }
    } else {
        header('Location: ?page=login');
    }
} else ?>
<div class="mx-auto w-25 mt-5">
    <form action="/?page=login" method="post">
        <h2 class="text-center">Log in</h2>
        <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Email" required="required">
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block w-25 mx-auto">Log in</button>
        </div>
    </form>


    <a href="/?page=signup" class="btn btn-success ml-auto d-inline-block">Create account</a>
    <div class="text-right">
        <?php echo $loginWithFb; ?>
    </div>
</div>