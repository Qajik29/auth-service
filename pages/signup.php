<?php
if(isset($_SESSION["authenticated"])){
    header('Location: ?page=dashboard');
}
?>
<div class="mx-auto w-25 mt-5">
    <form action="?page=handler" method="post">
        <h2 class="text-center">Sign up</h2>
        <div class="form-group">
            <input type="text" name="fname" class="form-control" placeholder="First name" required="required">
        </div>
        <div class="form-group">
            <input type="text" name="lname" class="form-control" placeholder="Last name" required="required">
        </div>
        <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Email" required="required">
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary btn-block w-25 mx-auto" value="Log in">
        </div>

    </form>
    <a href="/?page=login" class="btn btn-success ml-auto d-inline-block">Already have an account?</a>
    <?php
    if(isset($_SESSION["user_already_exists"])){
        echo '<h5 class="text-center mt-5 alert alert-warning">'.$_SESSION["user_already_exists"].'</h5>';
        unset($_SESSION["user_already_exists"]);
    }
    ?>
</div>