<?php
session_start();
require_once "pages/fbconfig.php";
require_once "functions.php";
$is_table_created = require_once "config/connection.php";
?>
<!DOCTYPE html>
<html xmlns:fb = "http://www.facebook.com/2008/fbml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Login page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/styles.css" rel="stylesheet" type="text/css" />
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '597415997663020',
                cookie     : true,
                xfbml      : true,
                version    : 'v5.0'
            });
            FB.AppEvents.logPageView();
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</head>
<body>
<?php
if($is_table_created){
    if(!isset($_GET['page'])){
        header("Location: ?page=login");
    }
    switch ($_GET['page']) {
        case 'login':
            require_once "pages/signin.php";
            break;
        case 'signup':
            require_once "pages/signup.php";
            break;
        case 'handler':
            require_once "pages/handler.php";
            break;
        case 'logout':
            require_once "pages/logout.php";
            break;
        case 'dashboard':
            require_once "pages/dashboard.php";
            break;
    }
}
?>
</body>
</html>