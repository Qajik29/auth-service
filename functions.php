<?php
if(!function_exists('clear_data')){
    function clear_data($data){
        return htmlspecialchars(stripslashes(trim($data)));
    }
}