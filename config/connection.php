<?php
if (!defined('HOST')) {
    define('HOST', 'localhost');
}
if (!defined('DBNAME')) {
    define('DBNAME', 'auth_db');
}
if (!defined('DBUSER')) {
    define('DBUSER', 'root');
}
if (!defined('DBPASS')) {
    define('DBPASS', '');
}
$table_created = false;
try {
    $pdo = new PDO("mysql:host=".HOST, DBUSER, DBPASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbname = "`".str_replace("`","``",DBNAME)."`";
    $pdo->query("CREATE DATABASE IF NOT EXISTS $dbname");
    $res = $pdo->query("use $dbname");
    if($res){
        $create_signin_table_query = 'CREATE TABLE IF NOT EXISTS `users` (
        `id` INT AUTO_INCREMENT NOT NULL,
        `fname` varchar(255) NOT NULL,
        `lname` varchar(255),
        `email` varchar(255),
        `password` varchar(255),
        PRIMARY KEY (`id`)) 
        CHARACTER SET utf8 COLLATE utf8_general_ci';
        $pdo->exec($create_signin_table_query);
        $table_created = true;
    }
}catch(PDOException $e) {
    echo $e->getMessage();
}

return $table_created;